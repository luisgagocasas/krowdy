# Luis Gago Casas
### Developer Full Stack JavaScript

- Mail: luisgago@lagc-peru.com
- WebSite: https://luisgagocasas.com
- Linkedin: https://linkedin.com/in/luisgagocasas/

## Project
- Backend: mongodb, nodejs
- Frontend: React, axios, Ant Design