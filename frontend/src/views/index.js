import LoginView from './login'
import UserView from './user'
import RegisterView from './register'

export {
  LoginView,
  UserView,
  RegisterView
}