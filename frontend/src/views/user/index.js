import React, { Component } from 'react';
import './styles.css';

import HeaderTop from '../../components/HeaderTop'
import Content from '../../components/Content'
import Footer from '../../components/Footer'

class UserView extends Component {
  componentDidMount() {
    if (!localStorage.getItem('token')) {
      this.props.history.push("/login");
    }
  }

  render() {
    return (
      <div>
        <HeaderTop protect={true} />
        <Content />
        <Footer />
      </div>
    );
  }
}

export default UserView;
