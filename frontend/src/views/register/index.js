import React, { Component } from 'react';
import {
  Form,
  Input,
  Button,
  Row,
  Col, } from 'antd';
import { BrowserRouter as Router, Link } from "react-router-dom";

import './styles.css';
import HeaderTop from '../../components/HeaderTop';
import Footer from '../../components/Footer';
import { ResourceSignup } from '../../config';

const FormItem = Form.Item;

class RegisterView extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      message: '',
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        ResourceSignup.post(values)
          .then((d) => {
            this.setState({
              message: 'Cuenta creada! ' + d.statusText,
            })
            this.props.history.push("/login");
          })
          .catch((error) => {
            this.setState({
              message: 'Se produjo un error :(',
            })
          });
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <HeaderTop protect={false} />
        <div className="container shadow-content padding-content color-white">
          <Row>
            <Col span={12} offset={6}>
              <Form onSubmit={this.handleSubmit} className="login-form">
                <div className="center">
                  <h2>Registro</h2>
                </div>
                <FormItem>
                  {getFieldDecorator('country', {
                    rules: [{ required: true, message: 'Por favor ingresa tu País!' }],
                    initialValue: 'Perú'
                  })(
                    <Input type="hidden" />
                  )}
                </FormItem>
                <FormItem>
                  {getFieldDecorator('phone', {
                    rules: [{ required: true, message: 'Por favor ingresa tu Número de telefono!' }],
                    initialValue: '12345678'
                  })(
                    <Input type="hidden" />
                  )}
                </FormItem>
                <FormItem>
                  {getFieldDecorator('displayName', {
                    rules: [{ required: true, message: 'Por favor ingresa tu nombre!' }],
                  })(
                    <Input placeholder="Nombre" />
                  )}
                </FormItem>
                <FormItem>
                  {getFieldDecorator('email', {
                    rules: [{ required: true, message: 'Por favor ingresa tu correo!' }],
                  })(
                    <Input placeholder="Correo Electronico" />
                  )}
                </FormItem>
                <FormItem>
                  {getFieldDecorator('password', {
                    rules: [{ required: true, message: 'Por favor ingresa una contraseña!' }],
                  })(
                    <Input type="password" placeholder="Contraseña" />
                  )}
                </FormItem>
                <FormItem className="center">
                  <Button type="primary" htmlType="submit" className="login-form-button">
                    Crear!
                  </Button>
                  <p>{this.state.message}</p>
                  <br />
                  o
                  <br /> <Link to="/login"> Entrar</Link>
                </FormItem>
              </Form>
            </Col>
          </Row>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Form.create()(RegisterView);
