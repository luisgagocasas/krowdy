import React, { Component } from 'react';
import {
  Form,
  Icon,
  Input,
  Button,
  Row,
  Col, } from 'antd';
import { BrowserRouter as Router, Link } from "react-router-dom";

import './styles.css';
import HeaderTop from '../../components/HeaderTop'
import Footer from '../../components/Footer'
import { ResourceSignin } from '../../config';

const FormItem = Form.Item;

class LoginView extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      message: '',
    }
  }

  componentDidMount() {
    if (localStorage.getItem('token')) {
      this.props.history.push("/user");
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        ResourceSignin.post(values)
          .then((d) => {
            localStorage.setItem('token', d.data.token);
            localStorage.setItem('displayName', d.data.user.name);
            localStorage.setItem('email', d.data.user.email);
            localStorage.setItem('id', d.data.user.id);
            this.setState({
              message: d.data.message,
            })
            this.props.history.push("/user");
          })
          .catch((error) => {
            this.setState({
              message: 'Se produjo un error :(',
            })
          });
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <HeaderTop protect={false} />
        <div className="container shadow-content padding-content color-white">
          <Row>
            <Col span={12} offset={6}>
              <Form onSubmit={this.handleSubmit} className="login-form">
                <div className="center">
                  <h2>Conéctate</h2>
                </div>
                <FormItem>
                  {getFieldDecorator('email', {
                    rules: [{ required: true, message: 'Por favor ingresa un usuario!' }],
                  })(
                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Correo" />
                  )}
                </FormItem>
                <FormItem>
                  {getFieldDecorator('password', {
                    rules: [{ required: true, message: 'Por favor ingresa una contraseña!' }],
                  })(
                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Contraseña" />
                  )}
                </FormItem>
                <FormItem className="center">
                  <Button type="primary" htmlType="submit" className="login-form-button">
                    Entrar
                  </Button>
                  <p>{this.state.message}</p>
                  <br />
                  o
                  <br /> <Link to="/register"> Registro!</Link>
                </FormItem>
              </Form>
            </Col>
          </Row>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Form.create()(LoginView);
