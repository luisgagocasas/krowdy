import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect
} from "react-router-dom";
import { LoginView, UserView, RegisterView } from './views';

import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path='/' component={()=>(<Redirect to='/login'/>)}/> 
          <Route path="/login" component={LoginView} />
          <Route path="/user" component={UserView} />
          <Route path="/register" component={RegisterView} />
        </div>
      </Router>
    );
  }
}

export default App;
