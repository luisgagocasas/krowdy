import React, { Component } from 'react';
import {
  Row,
  Col,
  Form,
  Button } from 'antd';
import Country from '../Country'
import { ResourceUser, ResourceCountry } from '../../config';

const FormItem = Form.Item;

class FormCountry extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      message: '',
      country: '',
      phone: ''
    }
  }

  componentDidMount() {
    ResourceCountry.get()
      .then((d) => {
        this.setState({
          country: d.data.country,
          phone: d.data.phone
        })
      })
  }


  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        ResourceUser.post(values.country)
          .then((d) => {
            this.setState({
              message: 'Se guardo! ',
            })
          })
          .catch((error) => {
            this.setState({
              message: 'Se presento un problema :(! ',
            })
          });
      }
    });
  }

  checkPhone = (rule, value, callback) => {
    if (value.phone > 0) {
      callback();
      return;
    }
    callback('El numero debe ser mayor que cero!');
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit} layout="vertical">
        <Row>
          <Col span={15}>
            <FormItem label="Pais">
              {getFieldDecorator('country', {
                initialValue: {
                  phone: this.state.phone,
                  country: this.state.country
                },
                rules: [{ validator: this.checkPhone }],
              })(<Country />)}
            </FormItem>
          </Col>
          <Col span={8} className="center">
            <Button type="primary" htmlType="submit" className="login-form-button">
              Guardar
            </Button>
          </Col>
        </Row>
        <p>Enviaremos un código a este número; lo necesitarás en el último paso.</p>
        <p>{this.state.message}</p>
      </Form>
    );
  }
}

export default Form.create()(FormCountry);
