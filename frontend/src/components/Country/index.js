import React, { Component } from 'react';
import { Input, Select } from 'antd';

const Option = Select.Option;

class Country extends Component {
  constructor(props) {
    super(props);

    const value = props.value || {};
    this.state = {
      country: value.country || 'Perú',
      phone: value.number || 0,
    };
  }

  componentWillReceiveProps(nextProps) {
    // Should be a controlled component.
    if ('value' in nextProps) {
      const value = nextProps.value;
      this.setState(value);
    }
  }

  handlePhoneChange = (e) => {
    const phone = parseInt(e.target.value || 0, 10);
    if (isNaN(phone)) {
      return;
    }
    if (!('value' in this.props)) {
      this.setState({ phone });
    }
    this.triggerChange({ phone });
  }

  handleCountryChange = (country) => {
    if (!('value' in this.props)) {
      this.setState({ country });
    }
    this.triggerChange({ country });
  }

  triggerChange = (changedValue) => {
    // Should provide an event to pass value to Form.
    const onChange = this.props.onChange;
    if (onChange) {
      onChange(Object.assign({}, this.state, changedValue));
    }
  }

  render() {
    const { size } = this.props;
    const state = this.state;
    return (
      <span>
        <Select
          value={state.country}
          size={size}
          style={{ width: '32%' }}
          onChange={this.handleCountryChange}
        >
          <Option value="Perú">Perú (+51 )</Option>
          <Option value="Bolivia">Bolivia (+591 )</Option>
        </Select>
        <Input
          type="text"
          size={size}
          value={state.phone}
          style={{ width: '60%', marginLeft: '4%' }}
          onChange={this.handlePhoneChange}
        />
      </span>
    );
  }
}

export default Country;