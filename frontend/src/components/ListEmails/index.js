import React, { Component } from 'react';
import {
  Row,
  Col,
  Form,
  Button,
  Input,
  List } from 'antd';
import { ResourceEmail } from '../../config';

const FormItem = Form.Item;

class ListEmail extends Component {
  constructor(props) {
    super(props);
    this.handleEmailDeleteClick = this.handleEmailDeleteClick.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.state = {
      data: [],
      messageCreate: '',
      dataID: ''
    }
  }

  componentDidMount() {
    ResourceEmail.get()
      .then((d) => {
        console.log('1 => ',d.data)
        this.setState({
          data: d.data,
        })
      })
  }

  handleEmailDeleteClick = (e) => {
    ResourceEmail.delete(`${e}/`)
      .then((d) => {
        this.setState({
          message: d.data.message,
        })
      })

    var array = [...this.state.data];
    var index = array.indexOf(e)
    array.splice(index, 1);
    this.setState({data: array});
  }

  handleFormSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        
        ResourceEmail.post(values)
        .then((d) => {
          this.setState({
              dataID: d.data._id,
              messageCreate: 'Se añadio un nuevo correo! ',
            })

            let newItem = {
              active: false,
              email: values.email,
              user: localStorage.getItem("id"),
              _id: this.state.dataID
            };

            this.setState((prevState) => {
              return {
                data: prevState.data.concat(newItem)
              };
            });
          })

        this.setState({
          message: 'Se añadio un nuevo correo! ',
          data: values
        })
        console.log('<=', values.email)
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <List
          itemLayout="horizontal"
          dataSource={this.state.data}
          renderItem={item => (
            <List.Item
              actions={[
              <div>
                <Button
                  type="dashed"
                  onClick={() => this.handleEmailDeleteClick(item._id)}
                  style={{ borderStyle: 'none' }}
                  >Eliminar</Button>
              </div>
              ]}>
              <List.Item.Meta
                title={item.email}
              />
            </List.Item>
          )}
        />
        <p>{this.state.message}</p>
        <Form onSubmit={this.handleFormSubmit} layout="horizontal">
          <Row>
            <Col span={10}>
              <FormItem>
                {getFieldDecorator('user', {
                  rules: [{ required: true, message: 'Por favor ingresa id!' }],
                  initialValue: localStorage.getItem("id")
                })(
                  <Input type="hidden" />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('active', {
                  rules: [{ required: true, message: 'Por favor ingresa un email!' }],
                  initialValue: false
                })(
                  <Input type="hidden" />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('email', {
                  rules: [{ required: true, message: 'Por favor ingresa un email!' }],
                })(
                  <Input placeholder="Ingresa tu correo electrónico" />
                )}
              </FormItem>
              <br />
              <Button type="primary" htmlType="submit" className="login-form-button">
                Guardar
              </Button>
              <p>{this.state.messageCreate}</p>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}

export default Form.create()(ListEmail);
