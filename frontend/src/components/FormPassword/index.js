import React, { Component } from 'react';
import {
  Row,
  Col,
  Form,
  Button,
  Input } from 'antd';
import { ResourcePasswordRestart } from '../../config';

const FormItem = Form.Item;

class FormPassword extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      message: '',
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {        
        ResourcePasswordRestart.post(values)
        .then((d) => {
          this.setState({
              message: 'Se cambio la contraseña! ',
            })
          })
          .catch((error) => {
            this.setState({
              message: 'Se presento un problema :(! ',
            })
          });
      }
    });
  }


  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Las contraseñas no coinciden!');
    } else {
      callback();
    }
  }

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit} layout="horizontal">
        <Row>
          <Col span={10}>
            <FormItem label="Contraseña actual">
              {getFieldDecorator('password_old', {
                rules: [{ required: true, message: 'Por favor ingresa tu contraseña actual!' }],
              })(
                <Input type="password" />
              )}
            </FormItem>
            <FormItem label="Contraseña nueva">
              {getFieldDecorator('password', {
                rules: [{
                  required: true, message: 'Ingrese nueva contraseña!',
                }, {
                  validator: this.validateToNextPassword,
                }],
              })(
                <Input type="password" />
              )}
            </FormItem>
            <FormItem label="Vuelve a escribir tu contraseña nueva">
              {getFieldDecorator('confirm', {
                rules: [{
                  required: true, message: 'Confirme la contraseña!',
                }, {
                  validator: this.compareToFirstPassword,
                }],
              })(
                <Input type="password" onBlur={this.handleConfirmBlur} />
              )}
            </FormItem>
            <br />
            <Button type="primary" htmlType="submit" className="login-form-button">
              Guardar
            </Button>
            <p>{this.state.message}</p>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Form.create()(FormPassword);
