import React, { Component } from 'react';
import {
  Row,
  Col,
  Form,
  Button,
  Input } from 'antd';
import { ResourceEmail } from '../../config';
import ListEmail from '../ListEmails'

const FormItem = Form.Item;

class FormEmail extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      message: '',
      data: []
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        
        // ResourceEmail.post(values)
        // .then((d) => {
        //   this.setState({
        //       message: 'Se añadio un nuevo correo! ',
        //     })
        //   })
        // this.setState({
        //   message: 'Se añadio un nuevo correo! ',
        //   data: values.email
        // })
        console.log('<=', values.email)
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div>
        <ListEmail newEmail={this.state.data} />
        <Form onSubmit={this.handleSubmit} layout="horizontal">
          <Row>
            <Col span={10}>
              <FormItem>
                {getFieldDecorator('user', {
                  rules: [{ required: true, message: 'Por favor ingresa id!' }],
                  initialValue: localStorage.getItem("id")
                })(
                  <Input type="hidden" />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('active', {
                  rules: [{ required: true, message: 'Por favor ingresa un email!' }],
                  initialValue: false
                })(
                  <Input type="hidden" />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('email', {
                  rules: [{ required: true, message: 'Por favor ingresa un email!' }],
                })(
                  <Input placeholder="Ingresa tu correo electrónico" />
                )}
              </FormItem>
              <br />
              <Button type="primary" htmlType="submit" className="login-form-button">
                Guardar
              </Button>
              <p>{this.state.message}</p>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}

export default Form.create()(FormEmail);
