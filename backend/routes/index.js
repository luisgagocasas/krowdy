'use strict'

const express = require('express');
const auth = require('../middlewares/auth');
const api = express.Router();

const userCtrl = require('../controllers/user');
const client = require('../controllers/client');

//Client
api.get('/client', (client.getClients));
api.get('/client/:clientId', (client.getClient));
api.post('/client', (client.saveClient));
api.delete('/client/:clientId', (client.deleteClient));

//user
api.post('/signup', userCtrl.signUp);
api.post('/signin', userCtrl.signIn);
api.post('/updatepassword/:userId', userCtrl.updatePassword);
api.get('/country/:userId', userCtrl.getUser);
api.post('/user/:userId', userCtrl.updateUser);
api.get('/user', (userCtrl.getUsers));
api.get('/private', auth, function(req, res) {
  res.status(200).send({message: 'tienes acceso'})
})


module.exports = api;
