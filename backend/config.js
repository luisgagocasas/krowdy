module.exports = {
  port: process.env.PORT || 3005,
  db: process.env.MONGODB_URI || 'mongodb://localhost:27017/krowdy18',
  SECRET_TOKEN: 'HolaToken'
}
