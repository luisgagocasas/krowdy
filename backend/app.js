'use strict'

const compression = require('compression');
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const api = require('./routes');

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'OPTIONS,GET,DELETE,HEAD,PUT,POST');
  res.header('Access-Control-Allow-Headers', 'appid, X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization');
  next();
});

app.use(compression({ level: 5 }));

//Static - save memory cache
app.use(express.static(__dirname + '/public', {
  maxAge: '2h'
}));

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use('/api', api);

//Router
app.get('/', (req, res) => {
  res.sendFile('/index.html');
});

//404
app.get('*', (req, res) => {
  res.redirect('/');
});

module.exports = app;
