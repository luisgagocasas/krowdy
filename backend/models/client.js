'use strict'

const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

//Client
const ClientSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: 'User', required: 'Campo requerido.'},
  active: { type: Boolean },
  email: { type: String },
});

//Client message error
ClientSchema.plugin(uniqueValidator);


module.exports = {
  Client: mongoose.model('Client', ClientSchema)
}
