'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');

const userSchema = new Schema({
  email: { type: String, unique: true, lowercase: true},
  displayName: String,
  country: String,
  phone: Number,
  password: { type: String, required: true},
  signupDate: { type: Date, default: Date.now()},
  lastLogin: Date
})

userSchema.pre('save', function(next){
  let user = this;

  bcrypt.genSalt(10, (err, salt)=>{
    if(err) return next(err)

    bcrypt.hash(user.password, salt, null, (err, hash) =>{
      if (err) return next(err)

      user.password = hash
      next();
    });
  });
});

userSchema.methods.comparePassword = function(candidatePassword, callback) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) return callback(err)
    callback(null, isMatch)
  })
}

userSchema.methods.bcryptPassword = function(candidatePassword, callback) {
  bcrypt.genSalt(10, (err, salt)=>{
    if(err) return next(err)

    bcrypt.hash(candidatePassword, salt, null, (err, hash) =>{
      if (err) return next(err)
      callback(null, hash)
    });
  });
}

module.exports = mongoose.model('User', userSchema);
