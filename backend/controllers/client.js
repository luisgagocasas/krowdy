'use strict'

const Model = require('../models/client');

function getClient(req, res) {
  let clientId = req.params.clientId;

  Model.Client.findById(clientId, (error, client) => {
    if (error) {
      return res.status(500).send({message: `Error al realizar peticion ${error}`});
    }
    if(!client) {
      return res.status(404).send({message: 'client empty!'});
    }

    res.status(200).send(client);
  });
};

function getClients(req, res) {
  Model.Client.find({}, (err, client) => {
    if(err) {
      return res.status(500).send({message: `Error al realizar peticion ${error}`});
    }
    if(!client) {
      return res.status(404).send({message: 'empty client'});
    }

    res.status(200).send(client);
  }).sort({_id:-1});
};

function saveClient (req, res) {
  let client = new Model.Client();

  client.user = req.body.user;
  client.active = req.body.active;
  client.email = req.body.email;

  client.save((err, clientStored) => {
    if (err) {
      return res.status(500).send({message: `${err}`});
    }

    return res.status(200).send(clientStored);
  });
}

function deleteClient(req, res) {
  let clientId = req.params.clientId;

  Model.Client.remove({
    _id: clientId
  }, (err) => {
    if(err) {
      return res.status(500).send({message: `${err}`});
    }

    return res.status(200).send({message: 'Se ha borrado correctamente.'});
  });
};

function updateClient(req, res) {
  let clientId = req.params.clientId;
  let update = req.body;

  Model.Client.findByIdAndUpdate(clientId, update, (err) => {
    if(err) {
      return res.status(500).send({message: err});
    }

    return res.status(200).send(update);
  })
};

module.exports = {
  getClients,
  getClient,
  updateClient,
  saveClient,
  deleteClient
};
