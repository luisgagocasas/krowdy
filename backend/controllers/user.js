'use strict'

const User = require('../models/user');
const servicios = require('../services')

function signUp(req, res){
  const user = new User({
    email: req.body.email,
    displayName: req.body.displayName,
    country: req.body.country,
    phone: req.body.phone,
    password: req.body.password
  })

  user.save((err) =>{
    if(err) {
      return res.status(500).send({ message: `Error al crear usuario ${err}`});
    }

    return res.status(200).send({ token: servicios.createToken(user) });
  })
}

function signIn(req,res) {
  User.findOne({ email: req.body.email }, function (err, user) {
    if (err) {
      return res.status(500).send({message: err});
    }

    if (user != null) {
      user.comparePassword(req.body.password, function (err,match) {
        if (err) {
          return res.status(500).send({message: err});
        }

        if (match) {
          res.status(200).send({
            message: 'Has iniciado sesión.',
            user: {'name': user.displayName, 'email': user.email, 'id': user._id},
            token: servicios.createToken(user)
          })
        } else {
          res.status(401).send({
            message: 'Datos incorrectos.'
          })
        }
      })
    }
    else {
      return res.status(404).send({message: 'No existe el usuario.'});
    }
  })
}

function getUsers(req, res) {
  User.find({}, (err, users) => {
    if(err) {
      return res.status(500).send({message: `Error al realizar peticion ${error}`});
    }
    if(!users) {
      return res.status(404).send({message: 'empty users'});
    }

    res.status(200).send(users);
  }).sort({_id:-1});
};

function updateUser(req, res) {
  let userId = req.params.userId;
  let update = req.body;

  User.findByIdAndUpdate(userId, update, (err) => {
    if(err) {
      return res.status(500).send({message: err});
    }

    return res.status(200).send(update);
  })
};

function updatePassword(req,res) {
  let userId = req.params.userId;
  let update = req.body;

  User.findOne({ _id: userId }, function (err, user) {
    if (err) {
      return res.status(500).send({message: err});
    }

    if (user != null) {
      user.comparePassword(req.body.password_old, function (err,match) {
        if (err) {
          return res.status(500).send({message: err});
        }

        if (match) {
          user.bcryptPassword(req.body.password, function (err,match) {
            User.findByIdAndUpdate(userId, {password: match}, (err) => {
              if(err) {
                return res.status(500).send({message: err});
              }

              return res.status(200).send(update);
            })
          })
        }
      })
    }
    else {
      return res.status(404).send({message: 'No existe el usuario.'});
    }
  })
}

function getUser(req, res) {
  let userId = req.params.userId;

  User.findById(userId, (error, client) => {
    if (error) {
      return res.status(500).send({message: `Error al realizar peticion ${error}`});
    }
    if(!client) {
      return res.status(404).send({message: 'client empty!'});
    }

    res.status(200).send(client);
  });
};

module.exports = {
  signUp,
  signIn,
  updateUser,
  getUsers,
  updatePassword,
  getUser
}
