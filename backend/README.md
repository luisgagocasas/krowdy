## Backend

backend project

### Usage
- Clone or fork this repository
- Make sure you have [node.js](https://nodejs.org/) installed version 5+
- run `yarn install` to install dependencies


Init mongo DB

```
sudo service mongod restart
```


## Developer

- run `yarn start` active backend.
- API REST in [`http://localhost:3002/api/`](http://localhost:3002/api/)